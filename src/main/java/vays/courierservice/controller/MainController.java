package vays.courierservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vays.courierservice.model.Order;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Past;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@Scope("singleton")
@Slf4j
@Validated
public class MainController {
    private JdbcTemplate jdbcTemplate;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public MainController(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired
    private ApplicationContext applicationContext;

    @GetMapping("/courier")
    public String courier(
            Model model
    ) {
        model.addAttribute("courierForm", new Order());
        return "courier";
    }

    @GetMapping("/operator")
    public String operator(
            @RequestParam(required = false)
            @Past
            String id,
            Model model
    ) {
        model.addAttribute("operatorForm", new Order());
        return "operator";
    }


    @PostMapping("/courier")
    public String courierFindOrder(@ModelAttribute Order order, Model model) {

        List<Order> orders = jdbcTemplate.query(
                "SELECT number, dateTaskLag FROM orders WHERE number = ?", new Object[]{order.getNumber()},
                (rs, rowNum) -> new Order(rs.getLong("number"), parseDateTime(rs.getString("dateTaskLag"))));

        String errorCreateTask = null;
        if (orders.isEmpty()) {
            errorCreateTask = "заявка с номером " + order.getNumber() + " не существует";
            model.addAttribute("errorCreateTask", errorCreateTask);
            return "courierResultError";
        }
        if (orders.get(0).getDateTaskLag() != null) {
            errorCreateTask = "Задание для заявки №" + order.getNumber() + " уже создано " + dateFormat.format(orders.get(0).getDateTaskLag());
            model.addAttribute("errorCreateTask", errorCreateTask);
            return "courierResultError";
        }


        jdbcTemplate.update("UPDATE orders SET dateTaskLag = ? WHERE number = ?", dateFormat.format(new Date()), order.getNumber());
        return "courierResultSuccess";

    }

    @PostMapping("/operator")
    public String operatorFindOrder(@ModelAttribute Order order, Model model) {

        List<Order> orders = jdbcTemplate.query(
                "SELECT number, dateTaskLag FROM orders WHERE number LIKE ? AND dateTaskLag IS NOT NULL", new String[]{"%" + order.getNumber() + "%"},
                (rs, rowNum) -> new Order(rs.getLong("number"), parseDateTime(rs.getString("dateTaskLag"))));

        model.addAttribute("orders", orders);

        return "operatorResult";
    }

    @PostConstruct
    private void init() {
        jdbcTemplate.execute("DROP TABLE orders IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE orders(number INT PRIMARY KEY , dateTaskLag DATETIME)");

        List<Object[]> data = new ArrayList<>();
        for (int i = 1; i < 100; ++i) {
            data.add(new Object[]{i});
        }
        jdbcTemplate.batchUpdate("INSERT INTO orders(number) VALUES (?)", data);

        log.error("init");
    }


    private Date parseDateTime(String in) {
        if (in == null) {
            return null;
        }

        Date date = null;
        try {
            date = dateFormat.parse(in);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return date;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String errorHandlerCommon(Model model, Exception e) {
        String stackTrace = ExceptionUtils.getStackTrace(e);
        model.addAttribute("stackTrace", stackTrace);
        model.addAttribute("httpStatus", HttpStatus.INTERNAL_SERVER_ERROR);
        return "error";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String errorHandlerConstraints(Model model, ConstraintViolationException e) {
        String stackTrace = e.getMessage();
        model.addAttribute("stackTrace", stackTrace);
        model.addAttribute("httpStatus", HttpStatus.BAD_REQUEST);
        return "error";
    }

    @PreDestroy
    public void destroy() {

        log.warn("destroy MainController");
    }


}
